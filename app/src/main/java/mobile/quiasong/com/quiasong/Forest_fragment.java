package mobile.quiasong.com.quiasong;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Collin on 12/2/2017.
 */

public class Forest_fragment
    extends Fragment{

    View myView;
    WebView myWebView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.forest_layout, container, false);
        myWebView = (WebView)myView.findViewById(R.id.forest_webview);
        WebSettings webSettings = myWebView.getSettings();
        myWebView.setWebChromeClient(new WebChromeClient());
        myWebView.setWebViewClient(new WebViewClient());
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl("http://quiasong.com/binarytree/input/index.html");

        super.onResume();
        return myView;
    }
}
/*
        myWebView.setWebViewClient(new WebViewClient() {
@Override
public void onPageFinished(WebView view, String url) {

        progressBar.setVisibility(View.GONE);
        }
        });
  */